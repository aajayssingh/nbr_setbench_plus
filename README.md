# Memory Reclamation via Neutralization

This repo contains the memory reclamation benchmark used for the experiments and techniques reported in the paper titled: "Simple, Fast and Widely Applicable Concurrent Memory Reclamation via Neutralization" accepted in IEEE TPDS 2023.

The repo at present contains 6 data structures and 11 state of the art memory reclamation algorithms.

gitrepo     : [https://gitlab.com/aajayssingh/nbr_setbench_plus]

zenodo DOI  : [10.5281/zenodo.10203082]
zenodo link : [https://zenodo.org/records/10203082]

### Data Structures
- Trevor Brown's external (a,b) tree
- David, Guerraoui and Trigonakis 's external binary search tree using ticket locks
- lazylist
- harris list
- harris michael list
- hash table using harris michael list buckets

### Memory reclamation algorithms
- debra
- hazard pointer
- quiescent state based reclamation
- RCU reclamation
- Interval based reclamation
- NBR
- NBR+
- Wait Free Eras
- CrystallineL
- CrystallineW
- Hazard Eras

### Code Structure:
- reclamation algorithms are in common/recordmgr/
- data structures are in /ds/
- harness for the benchmark with entry file is in microbench/
- All the executables are generated in microbench/bin/
- Experiments can be done from /nbr_experiments/. All users wishing to run experiments will just need to be in this directory.
- tools/ contains all the plotting and experiment scripts

### Extending the benchamrk
Users can add new data structures by following the existing data structures in directory: ds/
Users can add new reclamation algorithms along the lines of exitising algorithms in directory: common/recordmgr

> Credit: This repo was carved out of [setbench](https://gitlab.com/trbot86/setbench) of [Multicore Lab](https://mc.uwaterloo.ca/) to test and evaluate reclamation algorithms with neutralization based reclamation. Preliminary work on neutralization based reclamation was previously published in PPOPP21( https://zenodo.org/record/4295604 )

### Accessing the required hardware on Chameleoncloud for TPDS reproducability.
The detailed steps required for getting started are available in Learn/Documentation tab [ https://chameleoncloud.readthedocs.io/en/latest/getting-started/index.html#step-1-log-in-to-chameleon ]

Step 1 : Log in to Chameleon and join a project. Sign in using federated signin option (use your university email) to get an account.

Step 2 : Reserve a Large NUMA multicore machine to run the benchmark.
Once you are logged in to Chameleoncloud look for required hardware to run the benchmark, go to Experiment/Hardware discovery tab to find the required hardware. Currently, compute_nvdimm is the hardware that matches our requirements of large multicore NUMA machine. It has 224 threads and 4 CPUS (numa nodes) at site CHI@UC. 

Step3:Launch a baremetal instance of the reserved machine.
This requires one to follow the steps at:
https://chameleoncloud.readthedocs.io/en/latest/getting-started/index.html#launching-an-instance

NOTE: we will need to choose an OS image to install on the bare metal instance. For our purpose CC-Ubuntu20.04 is the one.

- In details tab, populate fields as per your project name and reservation.
- select source tab and choose CC-Ubuntu20.04 in the image list
- create your SSH key pair creds to access the reserved machine.
- after creation launch Instance.
- Before you can access your instance, you need to first assign a floating IP address - an IP address that is accessible over the public Internet. Go to the Floating IP dashboard by clicking on Network and Floating IPs in the sidebar.
- use allocate IP to project button to allocate an IP.
- select the allocated IP and use Associate button, that lets you select a port from the drop down of the dialogue box that appears.
- use follwing instructions to add the private key to you ssh identity at your local machine.
https://chameleoncloud.readthedocs.io/en/latest/getting-started/index.html#accessing-your-instance
- ssh cc@yourfotingip
- note first time loging takes ~10 mins.
- At this point the machine should be ready to follow the steps in rest of this readme file..

## 🏁 Getting Started

These instructions will get you a copy of the artifact up and running on your machine for development and testing purposes. This can be done in two ways: 1) use our docker provided image or 2) alternatively prepare your machine to run our artifact.

``` NOTE: To better reproduce results of NBR we suggest to run the artifact on a multicore NUMA machine with at least two NUMA nodes.```

## (I) Running on Docker [If you download the code from zenodo then it will contain a docker container]
* Install the latest version of Docker on your system. We tested the artifact with the Docker version 19.03.6, build 369ce74a3c. Instructions to install Docker may be found at https://docs.docker.com/engine/install/ubuntu/. Or you may refer to the "Installing Docker" section at the end of this README.

  To check the version of docker on your machine use: 

    ``` ~$ docker -v```

* First, download the benchmark.

If you downloaded the benchmark from zenodo link (follow these instructions for running the docker image else skip ahead to alternative way (II)):
* Find docker image named nbr_docker.tar.gz in nbr_setbench_plus/ directory. 
  And load the docker image with the following command.

    ```~$ sudo docker load -i nbr_docker.tar.gz ```
* Verify that image was loaded.

    ```~$ sudo docker images```
* start a docker container from the loaded image

    ```~$ sudo docker run --name nbr -i -t --privileged nbr_setbench_plus /bin/bash ```
* run ls to see several files/folders of the artifact: Dockerfile README.md, common, ds, install.sh, lib, microbench, nbr_experiments, tools. 

    ```~$ ls ```
If this succeeds you can move to the quick test section and skip the following section which discusses alternative ways to prepare your machine to run the artifact.

## (II) *Alternative Way:* Preparing Host Machine:
In case you may want to prepare the host machine itself to run the artifact locally follow these instructions. Or you have downloaded the code from git repo, then you will not have a docker image. So follow these instructions to build manually.

First, download the benchmark.

Install following dependencies on your x86 Linux machine to compile and run.

```
 Use your system's package manager to install:
 > build-essential dos2unix g++ libnuma-dev make numactl parallel python3 python3-pip time zip micro bc
```

```
 Use your pip3 to install:
 > numpy matplotlib pandas seaborn ipython ipykernel jinja2 colorama
```

### Installing Dependencies
Required packages can also be installed in two ways:

- [] **Alternative 1 (use install.sh):**
```
~$ cd nbr_setbench_plus
~$ ./install.sh
```

- [] **Alternative 2 (manually):**
```
Use the following commands: 

~$ sudo apt-get update

~$ sudo apt-get install -y build-essential dos2unix g++ libnuma-dev make numactl parallel \
 python3 python3-pip time zip bc

~$ pip3 install numpy matplotlib pandas seaborn ipython ipykernel jinja2 colorama
```

> Once the required software/packages are installed we are ready to run the experiments and generate the figures discussed in  the submitted version of the paper.

## 🔧 Quick Test
Until now, we have prepared the setup needed to compile and run the artifact. Now, let's do a quick test where we will compile, run and generate results to verify that the original experiment (described later) would work correctly.

We would run two types of experiments. First, experiment to evaluate throughput and second experiment to evaluate peak memory usage.

Change directory to nbr_setbench_plus (if you used the alternative way to prepare your machine to execute the artifact) otherwise if you are in the docker container you would already be in nbr_setbench_plus/ directory.

### Compile benchmark: 
1. cd to /nbr_experiments/
2. execute ./compile.sh

This will compile the benchmark and generate all the executables with a combination of 6 data structures and 11 (plus one with no reclamation) reclaimers in directory microbench/bin/

Users will now be able to run any experiments we reported in the manuscript.  

### Test Run, throughput and peak memory usage experiments: 
To quickly compile, run and see default results for throughput experiment follow these steps:

* *step1*. Assuming you are currently in nbr_setbench_plus, execute the following command:

    ```~$ cd nbr_experiments```.
* *step2*. Run the following command:
    : for quicktest
    ```~$ ./run_quicktest.sh```

This runs compiles benchmark, runs experiment for a single lazylist data structure on minimal run paramters and generates plots.

### To change experiment run parameters:
Each data structure has corresponding python file that defines the experiment: reclamation algorithms to be used, number of threads, workload (fraction of inserts and deletes), size of data structures, number of trials to be used.

For example for lazylist: nbr_exp_run_ll.py for DGT: nbr_exp_run_dgt.py. See data_dir_arr variable in runlists.sh, runtrees.sh, runhashtable.sh.

Users can change the following run parameters in define_experiment() function of these python files.

* RECLAIMER_ALGOS
* __trials
* TOTAL_THREADS
* INS_DEL_HALF : a value 25 will imply a workload of 25% inserts, 25% deletes and remaining (50%) lookups. 
* DS_SIZE

The Quick test uses default run parameters as can be seen in nbr_exp_run_ll_quicktest.py


**WARNING:** if you are running the experiment in the docker container **DO NOT** exit the terminal after the Quick test finishes as we would need to copy the generated figures on the host machine to be able to see them.  

### Analyze generated figures:
In case you chose to run the experiment on your system locally then you can simply find the figures in /nbr_setbench_plus/nbr_experiments/plots/generated_plots/ directory and analyse them.

Otherwise if you are running inside The Docker container follow below steps to fetch figures: 

To copy generated figures on your host machine copy the plots from the docker container to your host system by following these steps.

* Verify the name of the docker container. Use the following command which would give us the name of the loaded docker container under NAMES column which is 'nbr'.


    ```~$ sudo docker container ls```

Open a new terminal on the same machine. Move to any directory where you would want the generated plots to be copied (use cd). And execute the following command. 

* Copy the generated plots from the nbr_experiments/plots/generated_plots folder to your current directory.

    ```~$ sudo docker cp nbr:/nbr_setbench_plus/nbr_experiments/plots/generated_plots/ .```

Now you can analyse the generated plots.

* Each plot for throughput experiments follows a naming convention: throughput-[data structure name]-[number of updates]-[max size of data structure].png. For example, a plot showing throughput of lazylist with 25% inserts and 25% deletes is named as: throughput-herlihy_lazylist-u25-k2000.png.

* Similarly the plot for peak memory usage follows a naming convention: maxresident-[data structure name]-[number of updates]-[max size of data structure].png. For example, a plot showing peak memory usage of lazylist with 25% inserts and 25% deletes is named as: maxresident-herlihy_lazylist-u25-sz2000.png.

## 🔧 Running the tests with all configuration reported in submitted paper takes ~60 hrs:
To help users be able to run experiments relatively faster (~10-12 hrs) we trim  number of workloadtypes and data structure sizes. Specifically, run paramters for trees are by default set to have 2M size and 100% update workload (50%insert and 50% deletes) and run parameters for lists are by default set to 2K size and 100% updates. Users can change the run paramaters, as described in section above, to generate plots for all the configurations reported in the TPDS paper.


> **NOTE** Instead of running the following throughput and robustness experiment scripts one by one, user could run a single script `~$ ./run_onetouch.sh` in nbr_experiments/. This runs all the following scripts in one go. User could go and do other stuff and comeback after ~10-12hrs to collect the generated plots. It is possible to shorten the duration by changing the run parameters like number of threads as mentioned above.

### Throughput and peak memory usage experiments [not needed if run_onetouch.sh is executed]:

* *step1*. Assuming you are currently in nbr_setbench_plus, execute the following command:

    ```~$ cd nbr_experiments```.

> NOTE: To avoid recompiling and running the trials users can remove -c and r cmd parameters in .sh files. From the line :python3 ../tools/data_framework/run_experiment.py $exp_file -crdp.
c implies compile, r implies run, d implies generate data base, p implies plot data.

* *step2*. Run the following command:
    : for lists
    ```~$ ./runlists.sh```
    
    : for hashtable
    ```~$ ./runhashtable.sh```
    
    : for trees
    ```~$ ./runtrees.sh```

> **Warning**: Using a list size more than 20K will take long time in prefilling the list. Therefore, we suggest to use a list size of less than or equal to 20K. 

 For the figures in the submitted paper we tested NBR on a NUMA machine with the following configuration:
    
    MACHINE1 (on Chameleon cloud)
    * Architecture        : Intel(R) Xeon(R) Platinum 8276 CPU @ 2.20GHz
    * CPU(s)              : 224
    * Sockets(s)          : 4
    * Thread(s) per core  : 2
    * Core(s) per socket  : 28
    * Memory              : 2.9T

    MACHINE2
    * Architecture        : Intel Xeon x86_64 CPU @2.1GHz
    * CPU(s)              : 192
    * Sockets(s)          : 4
    * Thread(s) per core  : 2
    * Core(s) per socket  : 24
    * Memory              : 377G

Note: as long as the nbr_setbench_plus is run on a the similar machine the generated plots should match the expected plots.

### Evaluate robustness (peak memory usage with thread delays) [not needed if run_onetouch.sh is executed]: 

* *step1*. Assuming you are currently in nbr_setbench_plus, execute the following command:

    ```~$ cd nbr_experiments```.
* *step2*. Run the following command:
    
    ```~$ ./run_memusage.sh```

### ⛏️ Analyze generated figures:

Once the above test completes the resultant figures could be found in nbr_experiments/plots/generated_plots. All plots follow the naming convention mentioned in the quick test section.

We have put the expected figures for this experiment in the nbr_experiments/plots/expected_plots/ directory. Please copy this directory in the same way as we copied  nbr_experiments/plots/generated_plots/

* Copy the generated plots from the nbr_experiments/plots/expected_plots folder to your current directory.

    ```~$ sudo docker cp nbr:/nbr_setbench_plus/nbr_experiments/plots/expected_plots/ .```

Now you can analyse the generated plots and compare them with the expected plots assuming you have access to similar hardware.
The plots for a particular daa structure are in its own subdirectory. For example for lazylist, plost are in plot_data_ll/

plot_data_abt          : results for Brown's ab tree.
plot_data_dgt          : results for DGT external binary search tree with ticket locks
plot_data_dgttd        : results for DGT tree with thread delays (robustness experiment)
plot_data_hl           : results for lazy harris list
plot_data_hm           : results for lazy harris michael list
plot_data_hmht         : results for lazy harris michael list based hash table
plot_data_ll           : results for lazy list
plot_data_ll_quicktest : results for lazy list when quicktest bash script is run.



## 🎉 What does runlists.sh, runhashtable.sh and runtrees.sh do?

1. Compile the benchmark with reclamation algorithms and data structures.
2. Run all reclamation algorithms, for a sequence of threads (say, 1,2,4,8,16,32,48 ...), for varying workloads (say, 50% inserts 50% deletes, 25% inserts 25% deletes, and 5% inserts 5% deletes) for the specified data structure. One reclamation algorithm is run several times. Each run is called one step. For example, NBR+ executing with 32 threads for a workload type that has 50% inserts and 50% deletes is called one step in our experiments.
3. Produce figures in directory nbr_setbench_plus/nbr_experiments/plots/generated_plots.

## 🚀 Types of machines we evaluated NBR-setnbench_plus on:

* Smallest NUMA machine we have tested NBR has following configuration:
  * Architecture        : Intel x86_64
  * CPU(s)              : 8
  * Socket(s)           : 1
  * Thread(s) per core  : 2
  * Core(s) per socket  : 4
  * Memory              : 16G
* Largest NUMA machine we have tested NBR has following configuration:
  * Architecture        : Intel x86_64
  * CPU(s)              : 192
  * Socket(s)           : 4
  * Thread(s) per core  : 2
  * Core(s) per socket  : 24
  * Memory              : 377G

## 🎉 Claims from the paper supported by the artifact:
- *claim 1*. NBR+ is faster than other reclamation algorithms considered in the paper.
  - please check throughput plots in nbr_setbench_plus/nbr_experiments/plots/generated_plots.
  - On our 192 CPUs and 4 sockets machine with 377G memory NBR+ has better throughput after 72 threads than other reclamation algorithms. 
- *claim 2*. NBR+ has bounds peak memory usage in presence of stalled threads.
  - please check mem-usage plots in nbr_setbench_plus/nbr_experiments/plots/generated_plots
  - Also memory usage with thread delays can be found at plots/generated_plots/plot_data_dgttd/
  - On our 192 CPUs and 4 sockets machine with 377G memory NBR+ has significantly lower peak memory consumption in comparison to non-robust reclamation algorithms like DEBRA whose memory consumption shoots up when threads stall.

## ✍️ References
1. https://gitlab.com/trbot86/setbench
2. https://mc.uwaterloo.ca/code.html

3. A. Singh, T. Brown, and A. Mashtizadeh, “Nbr: neutralization based
reclamation,” in Proceedings of the 26th ACM SIGPLAN Symposium on
Principles and Practice of Parallel Programming, 2021, pp. 175–190.

4. David, T., Guerraoui, R., & Trigonakis, V. (2015). Asynchronized concurrency: The secret to scaling concurrent search data structures. ACM SIGARCH Computer Architecture News, 43(1), 631-644.

5. Heller, S., Herlihy, M., Luchangco, V., Moir, M., Scherer, W. N., & Shavit, N. (2005, December). A lazy concurrent list-based set algorithm. In International Conference On Principles Of Distributed Systems (pp. 3-16). Springer, Berlin, Heidelberg.

6. T. L. Harris, “A pragmatic implementation of non-blocking linked-
lists,” in International Symposium on Distributed Computing. Springer,
2001, pp. 300–314.

7. T. Brown, “Techniques for constructing efficient lock-free data struc-
tures,” arXiv preprint arXiv:1712.05406, 2017. 

8. “Crystalline: Fast and memory efficient wait-free reclamation,”
R. Nikolaev and B. Ravindran, Eds., vol. abs/2108.02763, 2021.
[Online]. Available: https://arxiv.org/abs/2108.02763

9. https://github.com/urcs-sync/Interval-Based-Reclamation

10. https://gitlab.com/aajayssingh/nbr_setbench






## Installing Docker
Please follow these commands in order:

``` ~$ sudo apt update```

``` ~$ sudo apt-get install curl apt-transport-https ca-certificates software-properties-common ```

``` ~$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - ```

``` ~$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"  ``` 

``` ~$ sudo apt update ```

``` ~$ sudo apt install docker-ce  ```

verify installation:

``` ~$ docker -v ```

## Misc:

### Build Docker image
``` sudo docker build -t nbr_setbench_plus . ```

### Save docker image
``` sudo docker save nbr_setbench_plus:latest | gzip > nbr_docker.tar.gz ```

### erase all docker containers in the system
``` sudo docker system prune -a ```
