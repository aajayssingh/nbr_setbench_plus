how to easily run and plot graphs for your experiments:
compile: ./compile.sh
run binaries from /bin dir

# script I use to quickly visulaize stats on terminal

ds=harris_list;rm data_jax.csv;cols="%12s %12s %12s %12s %12s\n"; printf "$cols" alg nthreads tput tot_rec_ev max_res; for n in 64 128 190; do for alg in 2geibr; do avgtput=0 ; for ((trial=0;trial<5;++trial)); do sfname=step_${alg}_${n}_${trial}.txt; LD_PRELOAD=../../lib/libjemalloc.so time -f "cmd:%C \nmemory:%M" -o timetemp.txt ./ubench_${ds}.alloc_new.reclaim_${alg}.pool_none.out -nwork $n -nprefill $n -i 50 -d 40 -rq 0 -rqsize 1 -k 100 -nrq 0 -t 10000 > ${sfname} ; tput=`cat ${sfname} | grep "total throughput" | cut -d":" -f2 | tr -d " "`; numsig=`cat ${sfname} | grep "sum_num_signal_events_total" |cut -d"=" -f2 | tr -d " "| tail -1`; max_resmem=`cat timetemp.txt | grep memory | tail -1 | cut -d":" -f2` ; printf "%12s %12d %12d %12d %12d\n" "$alg" "$n" "$tput" "$numsig" "$max_resmem" | tee templine.txt; cat templine.txt | paste -sd " " >> data_jax.csv; done; done ; echo "-------------------";done;



./ubench_harris_list.alloc_new.reclaim_2geibr.pool_none.out -nwork 30 -nprefill 30 -i 50 -d 50 -rq 0 -rqsize 1 -k 100 -nrq 0 -t 10000