#!/bin/bash

data_dir_arr=("data_ll" "data_hl" "data_hm")
exp_file_arr=("nbr_exp_run_ll.py" "nbr_exp_run_hl.py" "nbr_exp_run_hm.py")

length=${#data_dir_arr[@]}
for ((i=0;i<$length;i++)); do
    data_dir=${data_dir_arr[$i]}
    exp_file=${exp_file_arr[$i]}
    echo "############################################"
    echo "Executing and generating FIGURES for ${data_dir}..."
    echo "############################################"
    python3 ../tools/data_framework/run_experiment.py $exp_file -crdp
    mkdir plots/generated_plots/plot_$data_dir
    echo "copying FIGURES to plots/generated_plots/plot_$data_dir/ "
    cp $data_dir/*.png plots/generated_plots/plot_$data_dir/
done


# data_dir="data_ll"
# exp_file=nbr_exp_run_ll.py
# echo "############################################"
# echo "Executing and generating FIGURES for LL..."
# echo "############################################"
# python3 ../tools/data_framework/run_experiment.py $exp_file -p

# # echo "copying FIGURES to plots/expected_plots/ "
# # cp data/*.png plots/expected_plots/
# mkdir plots/generated_plots/plot_$data_dir
# echo "copying FIGURES to plots/generated_plots/plot_$data_dir/ "
# cp $data_dir/*.png plots/generated_plots/plot_$data_dir/


# data_dir="data_hl"
# exp_file=nbr_exp_run_hl.py
# echo "############################################"
# echo "Executing and generating FIGURES for HL..."
# echo "############################################"

# python3 ../tools/data_framework/run_experiment.py $exp_file -p

# # echo "copying FIGURES to plots/expected_plots/ "
# # cp data/*.png plots/expected_plots/
# mkdir plots/generated_plots/plot_$data_dir
# echo "copying FIGURES to plots/generated_plots/plot_$data_dir/ "
# cp $data_dir/*.png plots/generated_plots/plot_$data_dir/


# data_dir="data_hm"
# exp_file=nbr_exp_run_hm.py
# echo "############################################"
# echo "Executing and generating FIGURES for HM..."
# echo "############################################"

# python3 ../tools/data_framework/run_experiment.py $exp_file -p

# # echo "copying FIGURES to plots/expected_plots/ "
# # cp data/*.png plots/expected_plots/
# mkdir plots/generated_plots/plot_$data_dir
# echo "copying FIGURES to plots/generated_plots/plot_$data_dir/ "
# cp $data_dir/*.png plots/generated_plots/plot_$data_dir/

