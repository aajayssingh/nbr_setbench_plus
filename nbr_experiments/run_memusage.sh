#!/bin/bash
##this script runs compiles, runs and then produces nice figures using Setbenches tool framework for DGT for mem usage (thread delay) experiment.  


data_dir="data_dgttd"
exp_file=nbr_exp_run_dgttd.py
echo "############################################"
echo "Executing and generating FIGURES for Thread Delayed DGT..."
echo "############################################"

python3 ../tools/data_framework/run_experiment.py $exp_file -crdp
mkdir plots/generated_plots/plot_$data_dir
echo "copying FIGURES to plots/generated_plots/plot_$data_dir/ "
cp $data_dir/*.png plots/generated_plots/plot_$data_dir/

# data_dir="data_dgtntd"
# exp_file=nbr_exp_run_dgtntd.py
# echo "############################################"
# echo "Executing and generating FIGURES for DGT..."
# echo "############################################"

# python3 ../tools/data_framework/run_experiment.py $exp_file -p

# # echo "copying FIGURES to plots/expected_plots/ "
# # cp data/*.png plots/expected_plots/
# mkdir plots/generated_plots/plot_$data_dir
# echo "copying FIGURES to plots/generated_plots/plot_$data_dir/ "
# cp $data_dir/*.png plots/generated_plots/plot_$data_dir/