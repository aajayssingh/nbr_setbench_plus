#!/bin/bash

data_dir="data_ll_quicktest"
exp_file=nbr_exp_run_ll_quicktest.py
echo "############################################"
echo "Compiling benchmark, Executing and generating FIGURES for LL quicktest..."
echo "############################################"
python3 ../tools/data_framework/run_experiment.py $exp_file -crdp
mkdir plots/generated_plots/plot_$data_dir
echo "copying FIGURES to plots/generated_plots/plot_$data_dir/ "
cp $data_dir/*.png plots/generated_plots/plot_$data_dir/