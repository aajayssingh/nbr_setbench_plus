#!/bin/bash

data_dir="data_hmht"
exp_file=nbr_exp_run_hmht.py
echo "############################################"
echo "Executing and generating FIGURES for HMHT..."
echo "############################################"

python3 ../tools/data_framework/run_experiment.py $exp_file -crdp
mkdir plots/generated_plots/plot_$data_dir
echo "copying FIGURES to plots/generated_plots/plot_$data_dir/ "
cp $data_dir/*.png plots/generated_plots/plot_$data_dir/