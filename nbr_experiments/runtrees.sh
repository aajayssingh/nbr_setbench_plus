#!/bin/bash

data_dir_arr=("data_dgt" "data_abt")
exp_file_arr=("nbr_exp_run_dgt.py" "nbr_exp_run_abt.py")

length=${#data_dir_arr[@]}
for ((i=0;i<$length;i++)); do
    data_dir=${data_dir_arr[$i]}
    exp_file=${exp_file_arr[$i]}
    echo "############################################"
    echo "Executing and generating FIGURES for ${data_dir}..."
    echo "############################################"
    python3 ../tools/data_framework/run_experiment.py $exp_file -crdp
    mkdir plots/generated_plots/plot_$data_dir
    echo "copying FIGURES to plots/generated_plots/plot_$data_dir/ "
    cp $data_dir/*.png plots/generated_plots/plot_$data_dir/
done

# data_dir="data_dgt"
# exp_file=nbr_exp_run_dgt.py
# echo "############################################"
# echo "Executing and generating FIGURES for DGT..."
# echo "############################################"

# python3 ../tools/data_framework/run_experiment.py $exp_file -p

# # echo "copying FIGURES to plots/expected_plots/ "
# # cp data/*.png plots/expected_plots/
# mkdir plots/plot_$data_dir
# echo "copying FIGURES to plots/plot_$data_dir/ "
# cp $data_dir/*.png plots/plot_$data_dir/

# data_dir="data_abt"
# exp_file=nbr_exp_run_abt.py
# echo "############################################"
# echo "Executing and generating FIGURES for ABT..."
# echo "############################################"

# python3 ../tools/data_framework/run_experiment.py $exp_file -p

# echo "copying FIGURES to plots/expected_plots/ "
# cp data/*.png plots/expected_plots/
# mkdir plots/plot_$data_dir
# echo "copying FIGURES to plots/plot_$data_dir/ "
# cp $data_dir/*.png plots/plot_$data_dir/