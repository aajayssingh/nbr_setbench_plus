#!/bin/sh
##this script compiles the benchmark using Setbenche's tool framework.  
# -c flag just compiles the benchmark.
# I use "nbr_exp_run_ll.py" file as input, but also can use any other data structure specific file like "nbr_exp_run_dgt.py" to compile the benchmark.

# OUTPUT: Compiles the benchmark using the makefile in /microbench/. All executables are produced in /microbench/bin.
# Alternatively, for dev and debug purposes users can directly compile the microbench using the makefile in /microbench and /microbench/compile.sh



# echo " "
# echo "############################################"
# echo "Compiling benchmark..."
# echo "############################################"

python3 ../tools/data_framework/run_experiment.py nbr_exp_run_ll.py -c